use gdk::RGBA;

pub const FINAL_GRAVITY_IDEAL: f64 = 1.01627;
// when finalGravity is unknown, this constant is ideal. it is the average of all BJCP styles

pub const FINAL_BRIX_IDEAL: f64 = 4.1480675;
// when finalBrix is unknown, this constant is ideal. it is the average of all BJCP styles


pub const ZERO_RGBA: RGBA = RGBA {
    red: 255.0,
    green: 255.0,
    blue: 255.0,
    alpha: 1.0,
};