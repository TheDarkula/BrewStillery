#[derive(PartialEq, Clone, Copy, Debug)]
pub enum imperialOrMetric {
    ImperialGB,
    ImperialUS,
    Metric,
}

#[derive(PartialEq, Clone, Copy, Debug)]
pub enum grainTypes {
    TwoRow,
    SixRow,
    Black,
    Caramel60,
    Caramel80,
    Caramel120,
    Chocolate,
    Corn,
    Dextrine,
    Oat,
    Rice,
    Rye,
    Wheat,
}

#[derive(PartialEq, Clone, Copy, Debug)]
pub enum glassTypes {
    DimplePint,
    NonickPint,
    TulipPint,
    Pilsner,
    // We will switch to proper characters when this issue is stabilised:
    // https://github.com/rust-lang/rust/issues/28979
    // Maß,
    Mafs,
    DimpleHalfPint,
    NonickHalfPint,
    TulipHalfPint,
}