use gtk;
use gtk::ComboBoxExt;
use std::fmt;
use std::str::FromStr;
use crate::enums::generalEnums::{imperialOrMetric, grainTypes, glassTypes};

impl FromStr for imperialOrMetric {
    type Err = String;

    fn from_str(inputString: &str) -> Result<Self, Self::Err> {
        match inputString {
            "imperialGB" => Ok(imperialOrMetric::ImperialGB),
            "imperialUS" => Ok(imperialOrMetric::ImperialUS),
            "metric" => Ok(imperialOrMetric::Metric),
            _ => Err(String::from("A ComboBoxText with imperialOrMetric has a misspelling")),
        }
    }
}

pub trait comboBoxTextImperialOrMetric {
    fn imperialOrMetricFromComboBoxText(&self) -> Result<imperialOrMetric, String>;
}

impl comboBoxTextImperialOrMetric for gtk::ComboBoxText {
    fn imperialOrMetricFromComboBoxText(&self) -> Result<imperialOrMetric, String> {
        self.get_active_id()
        .expect("In a call to imperialOrMetricFromComboBoxText(), .get_active_id() failed")
        .parse::<imperialOrMetric>()
    }
}

impl fmt::Display for grainTypes {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            grainTypes::TwoRow => write!(formatter, "2-Row"),
            grainTypes::SixRow => write!(formatter, "6-Row"),
            grainTypes::Black => write!(formatter, "Black"),
            grainTypes::Caramel60 => write!(formatter, "Caramel 60"),
            grainTypes::Caramel80 => write!(formatter, "Caramel 80"),
            grainTypes::Caramel120 => write!(formatter, "Caramel 120"),
            grainTypes::Chocolate => write!(formatter, "Chocolate"),
            grainTypes::Corn => write!(formatter, "Corn"),
            grainTypes::Dextrine => write!(formatter, "Dextrine"),
            grainTypes::Oat => write!(formatter, "Oat"),
            grainTypes::Rice => write!(formatter, "Rice"),
            grainTypes::Rye => write!(formatter, "Rye"),
            grainTypes::Wheat => write!(formatter, "Wheat"),
        }
    }
}

impl grainTypes {
    pub fn grainGravity(self) -> f64 {
        match self {
            grainTypes::TwoRow => 1.037,
            grainTypes::SixRow => 1.035,
            grainTypes::Black => 1.025,
            grainTypes::Caramel60 => 1.034,
            grainTypes::Caramel80 => 1.034,
            grainTypes::Caramel120 => 1.033,
            grainTypes::Chocolate => 1.034,
            grainTypes::Corn => 1.037,
            grainTypes::Dextrine => 1.033,
            grainTypes::Oat => 1.034,
            grainTypes::Rice => 1.032,
            grainTypes::Rye => 1.036,
            grainTypes::Wheat => 1.036,
        }
    }

    pub fn grainLovibond(self) -> f64 {
        match self {
            grainTypes::TwoRow => 1.8,
            grainTypes::SixRow => 1.8,
            grainTypes::Black => 500.0,
            grainTypes::Caramel60 => 60.0,
            grainTypes::Caramel80 => 80.0,
            grainTypes::Caramel120 => 120.0,
            grainTypes::Chocolate => 350.0,
            grainTypes::Corn => 1.3,
            grainTypes::Dextrine => 1.5,
            grainTypes::Oat => 1.0,
            grainTypes::Rice => 1.0,
            grainTypes::Rye => 2.0,
            grainTypes::Wheat => 1.6,
        }
    }
}

impl FromStr for grainTypes {
    type Err = String;

    fn from_str(inputString: &str) -> Result<Self, Self::Err> {
        match inputString {
            "2-Row" => Ok(grainTypes::TwoRow),
            "6-Row" => Ok(grainTypes::SixRow),
            "Black" => Ok(grainTypes::Black),
            "Caramel 60" => Ok(grainTypes::Caramel60),
            "Caramel 80" => Ok(grainTypes::Caramel80),
            "Caramel 120" => Ok(grainTypes::Caramel120),
            "Chocolate" => Ok(grainTypes::Chocolate),
            "Corn" => Ok(grainTypes::Corn),
            "Dextrine" => Ok(grainTypes::Dextrine),
            "Oat" => Ok(grainTypes::Oat),
            "Rice" => Ok(grainTypes::Rice),
            "Rye" => Ok(grainTypes::Rye),
            "Wheat" => Ok(grainTypes::Wheat),
            _ => Err(String::from("A ComboBoxText with grainTypes has a misspelling")),
        }
    }
}

pub trait comboBoxTextGrainTypes {
    fn grainTypesFromComboBoxText(&self) -> Result<grainTypes, String>;
}

impl comboBoxTextGrainTypes for gtk::ComboBoxText {
    fn grainTypesFromComboBoxText(&self) -> Result<grainTypes, String> {
        self.get_active_id()
        .expect("In a call to grainTypesFromComboBoxText(), .get_active_id() failed")
        .parse::<grainTypes>()
    }
}

impl fmt::Display for glassTypes {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            glassTypes::DimplePint => write!(formatter, "Dimple Pint"),
            glassTypes::NonickPint => write!(formatter, "Nonick Pint"),
            glassTypes::TulipPint => write!(formatter, "Tulip Pint"),
            glassTypes::Pilsner => write!(formatter, "Pilsner"),
            glassTypes::Mafs => write!(formatter, "Maß"),
            glassTypes::DimpleHalfPint => write!(formatter, "Dimple Half Pint"),
            glassTypes::NonickHalfPint => write!(formatter, "Nonick Half Pint"),
            glassTypes::TulipHalfPint => write!(formatter, "Tulip Half Pint"),
        }
    }
}

impl glassTypes {
    pub fn glassSize(self) -> f64 {
        match self {
            // Value is in centimeters
            glassTypes::DimplePint => 9.8,
            glassTypes::NonickPint => 9.0,
            glassTypes::TulipPint => 8.5,
            glassTypes::Pilsner => 8.0,
            // We will switch to proper characters when this issue is stabilised:
            // https://github.com/rust-lang/rust/issues/28979
            // glassTypes::Maß => 10.5,
            glassTypes::Mafs => 10.5,
            glassTypes::DimpleHalfPint => 8.0,
            glassTypes::NonickHalfPint => 7.0,
            glassTypes::TulipHalfPint => 7.2,
        }
    }
}

impl FromStr for glassTypes {
    type Err = String;

    fn from_str(inputString: &str) -> Result<Self, Self::Err> {
        match inputString {
            "Dimple Pint" => Ok(glassTypes::DimplePint),
            "Nonick Pint" => Ok(glassTypes::NonickPint),
            "Tulip Pint" => Ok(glassTypes::TulipPint),
            "Pilsner" => Ok(glassTypes::Pilsner),
            "Maß" => Ok(glassTypes::Mafs),
            "Dimple Half Pint" => Ok(glassTypes::DimpleHalfPint),
            "Nonick Half Pint" => Ok(glassTypes::NonickHalfPint),
            "Tulip Half Pint" => Ok(glassTypes::TulipHalfPint),
            _ => Err(String::from("A ComboBoxText with glassTypes has a misspelling")),
        }
    }
}

pub trait comboBoxTextglassTypes {
    fn glassTypesFromComboBoxText(&self) -> Result<glassTypes, String>;
}

impl comboBoxTextglassTypes for gtk::ComboBoxText {
    fn glassTypesFromComboBoxText(&self) -> Result<glassTypes, String> {
        self.get_active_id()
        .expect("In a call to glassTypesFromComboBoxText(), .get_active_id() failed")
        .parse::<glassTypes>()
    }
}