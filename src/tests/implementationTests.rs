use std::f64::NAN;
use crate::functions::commonFunctions::{inputMatching, singleInput};
use crate::enums::generalEnums::{imperialOrMetric, grainTypes, glassTypes};
use approx::ulps_eq;

#[test]
fn validInputTest() {
    let emptyString: &str = "";
    let validString: &str = "3.0";
    assert_eq!(emptyString.validInput().is_nan(), NAN.is_nan());
    ulps_eq!(validString.validInput(), 3.0);
}

#[test]
fn grainGravityTest() {
    let twoRow: grainTypes = grainTypes::TwoRow;
    let sixRow: grainTypes = grainTypes::SixRow;
    let black: grainTypes = grainTypes::Black;
    let caramelSixty: grainTypes = grainTypes::Caramel60;
    let caramelEighty: grainTypes = grainTypes::Caramel80;
    let caramelOneTwenty: grainTypes = grainTypes::Caramel120;
    let chocolate: grainTypes = grainTypes::Chocolate;
    let corn: grainTypes = grainTypes::Corn;
    let dextrine: grainTypes = grainTypes::Dextrine;
    let oat: grainTypes = grainTypes::Oat;
    let rice: grainTypes = grainTypes::Rice;
    let wheat: grainTypes = grainTypes::Wheat;

    ulps_eq!(twoRow.grainGravity(), 1.037);
    ulps_eq!(sixRow.grainGravity(), 1.035);
    ulps_eq!(black.grainGravity(), 1.025);
    ulps_eq!(caramelSixty.grainGravity(), 1.034);
    ulps_eq!(caramelEighty.grainGravity(), 1.034);
    ulps_eq!(caramelOneTwenty.grainGravity(), 1.033);
    ulps_eq!(chocolate.grainGravity(), 1.034);
    ulps_eq!(corn.grainGravity(), 1.037);
    ulps_eq!(dextrine.grainGravity(), 1.033);
    ulps_eq!(oat.grainGravity(), 1.034);
    ulps_eq!(rice.grainGravity(), 1.032);
    ulps_eq!(wheat.grainGravity(), 1.036);
}

#[test]
fn grainLovibondTest() {
    let twoRow: grainTypes = grainTypes::TwoRow;
    let sixRow: grainTypes = grainTypes::SixRow;
    let black: grainTypes = grainTypes::Black;
    let caramelSixty: grainTypes = grainTypes::Caramel60;
    let caramelEighty: grainTypes = grainTypes::Caramel80;
    let caramelOneTwenty: grainTypes = grainTypes::Caramel120;
    let chocolate: grainTypes = grainTypes::Chocolate;
    let corn: grainTypes = grainTypes::Corn;
    let dextrine: grainTypes = grainTypes::Dextrine;
    let oat: grainTypes = grainTypes::Oat;
    let rice: grainTypes = grainTypes::Rice;
    let wheat: grainTypes = grainTypes::Wheat;

    ulps_eq!(twoRow.grainLovibond(), 1.8);
    ulps_eq!(sixRow.grainLovibond(), 1.8);
    ulps_eq!(black.grainLovibond(), 500.0);
    ulps_eq!(caramelSixty.grainLovibond(), 60.0);
    ulps_eq!(caramelEighty.grainLovibond(), 80.0);
    ulps_eq!(caramelOneTwenty.grainLovibond(), 120.0);
    ulps_eq!(chocolate.grainLovibond(), 350.0);
    ulps_eq!(corn.grainLovibond(), 1.3);
    ulps_eq!(dextrine.grainLovibond(), 1.5);
    ulps_eq!(oat.grainLovibond(), 1.0);
    ulps_eq!(rice.grainLovibond(), 1.0);
    ulps_eq!(wheat.grainLovibond(), 1.6);
}

#[test]
fn glassSizeTest() {
    let dimplePint: glassTypes = glassTypes::DimplePint;
    let nonickPint: glassTypes = glassTypes::NonickPint;
    let tulipPint: glassTypes = glassTypes::TulipPint;
    let pilsner: glassTypes = glassTypes::Pilsner;
    let mafs: glassTypes = glassTypes::Mafs;
    let dimpleHalfPint: glassTypes = glassTypes::DimpleHalfPint;
    let nonickHalfPint: glassTypes = glassTypes::NonickHalfPint;
    let tulipHalfPint: glassTypes = glassTypes::TulipHalfPint;

    ulps_eq!(dimplePint.glassSize(), 9.8);
    ulps_eq!(nonickPint.glassSize(), 9.0);
    ulps_eq!(tulipPint.glassSize(), 8.5);
    ulps_eq!(pilsner.glassSize(), 8.0);
    ulps_eq!(mafs.glassSize(), 10.5);
    ulps_eq!(dimpleHalfPint.glassSize(), 8.0);
    ulps_eq!(nonickHalfPint.glassSize(), 7.0);
    ulps_eq!(tulipHalfPint.glassSize(), 7.2);
}

#[test]
fn imperialOrMetricFromStrTest() {
    assert_eq!("imperialGB".parse::<imperialOrMetric>(), Ok(imperialOrMetric::ImperialGB));
    assert_eq!("imperialUS".parse::<imperialOrMetric>(), Ok(imperialOrMetric::ImperialUS));
    assert_eq!("metric".parse::<imperialOrMetric>(), Ok(imperialOrMetric::Metric));
    assert_eq!("Something Incorrect".parse::<imperialOrMetric>(),
        Err(String::from("A ComboBoxText with imperialOrMetric has a misspelling")));
}

#[test]
fn grainTypesFromStrTest() {
    assert_eq!("2-Row".parse::<grainTypes>(), Ok(grainTypes::TwoRow));
    assert_eq!("6-Row".parse::<grainTypes>(), Ok(grainTypes::SixRow));
    assert_eq!("Black".parse::<grainTypes>(), Ok(grainTypes::Black));
    assert_eq!("Caramel 60".parse::<grainTypes>(), Ok(grainTypes::Caramel60));
    assert_eq!("Caramel 80".parse::<grainTypes>(), Ok(grainTypes::Caramel80));
    assert_eq!("Caramel 120".parse::<grainTypes>(), Ok(grainTypes::Caramel120));
    assert_eq!("Chocolate".parse::<grainTypes>(), Ok(grainTypes::Chocolate));
    assert_eq!("Corn".parse::<grainTypes>(), Ok(grainTypes::Corn));
    assert_eq!("Dextrine".parse::<grainTypes>(), Ok(grainTypes::Dextrine));
    assert_eq!("Oat".parse::<grainTypes>(), Ok(grainTypes::Oat));
    assert_eq!("Rice".parse::<grainTypes>(), Ok(grainTypes::Rice));
    assert_eq!("Rye".parse::<grainTypes>(), Ok(grainTypes::Rye));
    assert_eq!("Wheat".parse::<grainTypes>(), Ok(grainTypes::Wheat));
    assert_eq!("Something Incorrect".parse::<grainTypes>(),
        Err(String::from("A ComboBoxText with grainTypes has a misspelling")));
}

#[test]
fn grainTypesFmtTest() {
    assert_eq!(format!("{}", grainTypes::TwoRow), String::from("2-Row"));
    assert_eq!(format!("{}", grainTypes::SixRow), String::from("6-Row"));
    assert_eq!(format!("{}", grainTypes::Black), String::from("Black"));
    assert_eq!(format!("{}", grainTypes::Caramel60), String::from("Caramel 60"));
    assert_eq!(format!("{}", grainTypes::Caramel80), String::from("Caramel 80"));
    assert_eq!(format!("{}", grainTypes::Caramel120), String::from("Caramel 120"));
    assert_eq!(format!("{}", grainTypes::Chocolate), String::from("Chocolate"));
    assert_eq!(format!("{}", grainTypes::Corn), String::from("Corn"));
    assert_eq!(format!("{}", grainTypes::Dextrine), String::from("Dextrine"));
    assert_eq!(format!("{}", grainTypes::Oat), String::from("Oat"));
    assert_eq!(format!("{}", grainTypes::Rice), String::from("Rice"));
    assert_eq!(format!("{}", grainTypes::Rye), String::from("Rye"));
    assert_eq!(format!("{}", grainTypes::Wheat), String::from("Wheat"));
}

#[test]
fn glassTypesFromStrTest() {
    assert_eq!("Dimple Pint".parse::<glassTypes>(), Ok(glassTypes::DimplePint));
    assert_eq!("Nonick Pint".parse::<glassTypes>(), Ok(glassTypes::NonickPint));
    assert_eq!("Tulip Pint".parse::<glassTypes>(), Ok(glassTypes::TulipPint));
    assert_eq!("Pilsner".parse::<glassTypes>(), Ok(glassTypes::Pilsner));
    assert_eq!("Maß".parse::<glassTypes>(), Ok(glassTypes::Mafs));
    assert_eq!("Dimple Half Pint".parse::<glassTypes>(), Ok(glassTypes::DimpleHalfPint));
    assert_eq!("Nonick Half Pint".parse::<glassTypes>(), Ok(glassTypes::NonickHalfPint));
    assert_eq!("Tulip Half Pint".parse::<glassTypes>(), Ok(glassTypes::TulipHalfPint));
    assert_eq!("Something Incorrect".parse::<glassTypes>(),
        Err(String::from("A ComboBoxText with glassTypes has a misspelling")));
}

#[test]
fn glassTypesFmtTest() {
    assert_eq!(format!("{}", glassTypes::DimplePint), String::from("Dimple Pint"));
    assert_eq!(format!("{}", glassTypes::NonickPint), String::from("Nonick Pint"));
    assert_eq!(format!("{}", glassTypes::TulipPint), String::from("Tulip Pint"));
    assert_eq!(format!("{}", glassTypes::Pilsner), String::from("Pilsner"));
    assert_eq!(format!("{}", glassTypes::Mafs), String::from("Maß"));
    assert_eq!(format!("{}", glassTypes::DimpleHalfPint), String::from("Dimple Half Pint"));
    assert_eq!(format!("{}", glassTypes::NonickHalfPint), String::from("Nonick Half Pint"));
    assert_eq!(format!("{}", glassTypes::TulipHalfPint), String::from("Tulip Half Pint"));
    assert_eq!(format!("{}", glassTypes::DimplePint), String::from("Dimple Pint"));
}

#[test]
fn brixToGravityTest() {
    ulps_eq!(21.0.brixToGravity(), 1.0874528357576327);
}

#[test]
fn gravityToBrixTest() {
    ulps_eq!(1.0874528357576327.gravityToBrix(), 21.000000205000255);
}

#[test]
fn gravityToPlatoTest() {
    ulps_eq!(1.014.gravityToPlato(), 3.5740240997679393);
}

#[test]
fn gramsToOuncesTest() {
    ulps_eq!(300.0.gramsToOunces(), 10.5821886);
}

#[test]
fn poundsToOuncesTest() {
    ulps_eq!(3.5.poundsToOunces(), 56.0);
}

#[test]
fn ouncesToPoundsTest() {
    ulps_eq!(16.0.ouncesToPounds(), 1.0);
}

#[test]
fn kilosToPoundsTest() {
    ulps_eq!(45.0.kilosToPounds(), 99.20801799);
}

#[test]
fn poundsToKilosTest() {
    ulps_eq!(99.20801799.poundsToKilos(), 45.00000000308674);
}

#[test]
fn gramsToKilogramsTest() {
    ulps_eq!(1000.0.gramsToKilograms(), 1.0);
}

#[test]
fn litresToGallonsGBTest() {
    ulps_eq!(75.0.litresToGallonsGB(), 16.4976936);
}

#[test]
fn litresToGallonsUSTest() {
    ulps_eq!(75.0.litresToGallonsUS(), 19.8129039);
}

#[test]
fn gallonsGBToLitresTest() {
    ulps_eq!(20.0.gallonsGBToLitres(), 90.9218);
}

#[test]
fn gallonsUSToLitresTest() {
    ulps_eq!(20.0.gallonsUSToLitres(), 75.70823568);
}

#[test]
fn gallonsGBToGallonsUSTest() {
    ulps_eq!(20.0.gallonsGBToGallonsUS(), 24.019);
}

#[test]
fn gallonsUSToGallonsGBTest() {
    ulps_eq!(20.0.gallonsUSToGallonsGB(), 16.653484);
}

#[test]
fn sugarToHoneyTest() {
    ulps_eq!(1.0.sugarToHoney(), 1.219512195);
}

#[test]
fn volumeToSugarChampagneTest() {
    ulps_eq!(5.0.volumeToSugarChampagne(), 1.0);
}

#[test]
fn remainingOuncesTest() {
    ulps_eq!(1.2.remainingOunces(), 3.1999999999999993);
}