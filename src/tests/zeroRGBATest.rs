use gdk::RGBA;
use crate::constants::generalConstants::ZERO_RGBA;
use approx::ulps_eq;

#[test]
fn zeroRGBATest() {
    let new: RGBA = ZERO_RGBA;

    ulps_eq!(new.red, 255.0);
    ulps_eq!(new.green, 255.0);
    ulps_eq!(new.blue, 255.0);
    ulps_eq!(new.alpha, 1.0);
}