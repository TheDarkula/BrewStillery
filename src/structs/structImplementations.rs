use gtk::WidgetExt;
use crate::structs::generalStructs::colourOverlay;

impl colourOverlay {
    pub fn setOverlaysInvisible(&self) {
        self.dimplePint.set_opacity(0.0);
        self.nonickPint.set_opacity(0.0);
        self.tulipPint.set_opacity(0.0);
        self.pilsner.set_opacity(0.0);
        self.mafs.set_opacity(0.0);
        self.dimpleHalfPint.set_opacity(0.0);
        self.nonickHalfPint.set_opacity(0.0);
        self.tulipHalfPint.set_opacity(0.0);
    }
}