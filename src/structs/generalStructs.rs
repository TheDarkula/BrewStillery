use gtk;

#[derive(Clone, Debug)]
pub struct colourOverlay {
    pub overlay: gtk::Overlay,
    pub colourOutput: gtk::ColorButton,
    pub dimplePint: gtk::Image,
    pub nonickPint: gtk::Image,
    pub tulipPint: gtk::Image,
    pub pilsner: gtk::Image,
    pub mafs: gtk::Image,
    pub dimpleHalfPint: gtk::Image,
    pub nonickHalfPint: gtk::Image,
    pub tulipHalfPint: gtk::Image,
}

#[derive(Clone, Copy, Debug)]
pub struct LAB {
    pub reconstructedL: f64,
    pub reconstructedA: f64,
    pub reconstructedB: f64,
}

#[derive(Clone, Copy, Debug)]
pub struct XYZ {
    pub X: f64,
    pub Y: f64,
    pub Z: f64,
}

#[derive(Clone, Copy, Debug)]
pub struct realABVAndAttenuation {
    pub ABV: f64,
    pub attenuation: f64,
}