use gtk::EntryExt;
use crate::functions::commonFunctions::{inputMatching, singleInput};
use crate::enums::generalEnums::imperialOrMetric;
use crate::enums::enumImplementations::comboBoxTextImperialOrMetric;

pub fn champagneCarbonationPrep(champagneCarbonationBuilder: &gtk::Builder) {
    let champagneCarbonationVolume: gtk::Entry = champagneCarbonationBuilder
    .get_object("champagneCarbonationVolume")
    .expect("champagneCarbonationPrep(), champagneCarbonationVolume");

    let champagneCarbonationVolumeBuffer: String = champagneCarbonationVolume.get_text()
    .expect("champagneCarbonationPrep(), champagneCarbonationVolumeBuffer");

    let champagneVolume: f64 = champagneCarbonationVolumeBuffer.validInput();

    let champagneCarbonationSugar: gtk::Entry = champagneCarbonationBuilder
    .get_object("champagneCarbonationSugar")
    .expect("champagneCarbonationPrep(), champagneCarbonationSugar");

    let champagneCarbonationUnits: gtk::ComboBoxText = champagneCarbonationBuilder
    .get_object("champagneCarbonationUnits")
    .expect("champagneCarbonationPrep(), champagneCarbonationUnits");

    let champagneCarbonationUnitsEnum: imperialOrMetric = champagneCarbonationUnits
    .imperialOrMetricFromComboBoxText()
    .expect("champagneCarbonationPrep(), champagneCarbonationUnitsEnum");

    if champagneVolume.is_nan() {
        champagneCarbonationSugar.set_text("Enter a number");
    } else if champagneVolume <= 0.0 {
        champagneCarbonationSugar.set_text("Enter a positive number");
    } else {
        let totalSugar: f64 = champagneCarbonationMaths(champagneVolume, champagneCarbonationUnitsEnum);
        champagneOutput(totalSugar, champagneCarbonationUnitsEnum, champagneCarbonationBuilder)
    }
}

pub fn champagneCarbonationMaths(champagneVolume: f64,
    champagneCarbonationUnitsEnum: imperialOrMetric) -> f64 {

    match champagneCarbonationUnitsEnum {
        imperialOrMetric::ImperialGB => {
            champagneVolume.gallonsGBToGallonsUS().volumeToSugarChampagne()
        }
        imperialOrMetric::ImperialUS => {
            champagneVolume.volumeToSugarChampagne()
        }
        imperialOrMetric::Metric => {
            champagneVolume.litresToGallonsUS().volumeToSugarChampagne().poundsToKilos()
        }
    }
}

pub fn champagneCarbonationFormatting(totalSugar: f64,
    champagneCarbonationUnitsEnum: imperialOrMetric) -> String {

    match champagneCarbonationUnitsEnum {
        imperialOrMetric::ImperialGB | imperialOrMetric::ImperialUS => {
            if totalSugar < 0.995 {
                format!("{:.2} oz", totalSugar.remainingOunces())
            } else if totalSugar >= 0.995 && totalSugar < 1.005 {
                format!("{:.0} lb", totalSugar)
            } else if totalSugar.trunc() as i64 == 1 && totalSugar.remainingOunces() < 15.995 {
                format!("{:.0} lb {:.2} oz", totalSugar.trunc(), totalSugar.remainingOunces())
            } else if totalSugar.fract() == 0.0 || totalSugar.remainingOunces() < 0.005 ||
            totalSugar.remainingOunces() >= 15.995 {
                format!("{:.0} lbs", totalSugar)
            } else {
                format!("{:.0} lbs {:.2} oz", totalSugar.trunc(), totalSugar.remainingOunces())
            }
        }
        imperialOrMetric::Metric => {
            if totalSugar >= 0.995 && totalSugar < 1.005 {
                format!("{:.0} kilo", totalSugar)
            } else if totalSugar.fract() >= 0.995 || totalSugar.fract() < 0.005 {
                format!("{:.0} kilos", totalSugar)
            } else {
                format!("{:.2} kilos", totalSugar)
            }
        }
    }
}

fn champagneOutput(totalSugar: f64, champagneCarbonationUnitsEnum: imperialOrMetric,
    champagneCarbonationBuilder: &gtk::Builder) {

    let champagneCarbonationSugar: gtk::Entry = champagneCarbonationBuilder
    .get_object("champagneCarbonationSugar")
    .expect("champagneCarbonationPrep(), champagneCarbonationSugar");

    let sugar: String = champagneCarbonationFormatting(totalSugar, champagneCarbonationUnitsEnum);

    champagneCarbonationSugar.set_text(&sugar);
}