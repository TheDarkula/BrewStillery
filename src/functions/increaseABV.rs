use gtk::EntryExt;
use rayon::iter::{IntoParallelIterator, ParallelIterator};
use crate::functions::commonFunctions::{inputMatching, singleInput, realABVAndAttenuation};
use crate::constants::generalConstants::FINAL_BRIX_IDEAL;
use crate::enums::generalEnums::imperialOrMetric;
use crate::enums::enumImplementations::comboBoxTextImperialOrMetric;

#[derive(Clone, Copy, Debug)]
pub struct increaseABVData {
    pub startingBrix: f64,
    pub desiredABV: f64,
    pub currentVolume: f64,
    pub increaseABVUnitsEnum: imperialOrMetric,
}

#[derive(Clone, Copy, Debug)]
pub struct finalSugarFloat {
    pub newStartingBrixFinal: f64,
    pub sugarToAdd: f64,
    pub honeyToAdd: f64,
}

#[derive(Debug)]
pub struct sugarOutputStrings {
    pub newStartingBrixFinal: String,
    pub sugarAmount: String,
    pub honeyAmount: String,
}

pub fn increaseABVPrep(increaseABVBuilder: &gtk::Builder) {
    let increaseABVStartingBrix: gtk::Entry = increaseABVBuilder
    .get_object("increaseABVStartingBrix")
    .expect("increaseABVPrep(), increaseABVStartingBrix");

    let increaseABVBrixBuffer: String = increaseABVStartingBrix.get_text()
    .expect("increaseABVPrep(), increaseABVBrixBuffer");

    let increaseABVDesiredABV: gtk::Entry = increaseABVBuilder.get_object("increaseABVDesiredABV")
    .expect("increaseABVPrep(), increaseABVDesiredABV");

    let increaseABVABVBuffer: String = increaseABVDesiredABV.get_text()
    .expect("increaseABVPrep(), increaseABVABVBuffer");

    let increaseABVCurrentVolume: gtk::Entry = increaseABVBuilder
    .get_object("increaseABVCurrentVolume")
    .expect("increaseABVPrep(), increaseABVCurrentVolume");

    let increaseABVVolumeBuffer: String = increaseABVCurrentVolume.get_text()
    .expect("increaseABVPrep(), increaseABVVolumeBuffer");

    let increaseABVFinalBrix: gtk::Entry = increaseABVBuilder.get_object("increaseABVFinalBrix")
    .expect("increaseABVPrep(), increaseABVFinalBrix");

    let increaseABVSugar: gtk::Entry = increaseABVBuilder.get_object("increaseABVSugar")
    .expect("increaseABVPrep(), increaseABVSugar");

    let increaseABVHoney: gtk::Entry = increaseABVBuilder.get_object("increaseABVHoney")
    .expect("increaseABVPrep(), increaseABVHoney");

    let increaseABVUnits: gtk::ComboBoxText = increaseABVBuilder.get_object("increaseABVUnits")
    .expect("increaseABVPrep(), increaseABVUnits");

    let increaseABVUnitsEnum: imperialOrMetric = increaseABVUnits
    .imperialOrMetricFromComboBoxText()
    .expect("increaseABVPrep(), increaseABVUnitsEnum");

    let allInputs = increaseABVData {
        startingBrix: increaseABVBrixBuffer.validInput(),
        desiredABV: increaseABVABVBuffer.validInput(),
        currentVolume: increaseABVVolumeBuffer.validInput(),
        increaseABVUnitsEnum: increaseABVUnitsEnum,
    };

    if allInputs.startingBrix < 4.1480675 {
        increaseABVFinalBrix.set_text("Enter a brix greater than 4.1480675");
        increaseABVSugar.set_text("");
        increaseABVHoney.set_text("");
    } else if allInputs.startingBrix > 32.0 {
        increaseABVFinalBrix.set_text("Enter a brix less than 32");
        increaseABVSugar.set_text("");
        increaseABVHoney.set_text("");
    } else if allInputs.desiredABV < 1.0 || allInputs.desiredABV > 26.809749938584304 {
        increaseABVFinalBrix.set_text("Enter an ABV between 1 and 26.809");
        increaseABVSugar.set_text("");
        increaseABVHoney.set_text("");
    } else if allInputs.startingBrix.is_nan() || allInputs.desiredABV.is_nan() ||
    allInputs.currentVolume.is_nan() {
        increaseABVFinalBrix.set_text("Enter all 3 inputs");
        increaseABVSugar.set_text("");
        increaseABVHoney.set_text("");
    } else if allInputs.desiredABV <= 0.0 || allInputs.currentVolume <= 0.0 {
        increaseABVFinalBrix.set_text("Enter a positive number");
        increaseABVSugar.set_text("");
        increaseABVHoney.set_text("");
    } else {
        increaseABVOutput(allInputs, increaseABVBuilder);
    }
}

pub fn increaseABVMaths(allInputs: &increaseABVData) -> finalSugarFloat {
    let mut newStartingBrix: f64 = allInputs.startingBrix;

    newStartingBrix = (((newStartingBrix * 1000.0) as u32)..33000)
        .into_par_iter()
        .map(|mapBrix| {
            let tempBrix: f64 = ( mapBrix as f64 / 1000.0 ) + 0.0001;
            let tempABV: f64 = realABVAndAttenuation(tempBrix,FINAL_BRIX_IDEAL).0;

            (tempBrix, tempABV)
        })
        .find_first(|(_tempBrix, tempABV)| {
            allInputs.desiredABV < *tempABV
        })
        .expect("increaseABVPrep(), newEstimatedABV")
        .0;


    let differenceBrix: f64 = newStartingBrix - allInputs.startingBrix;

    let mut finalOutputFloat: finalSugarFloat = finalSugarFloat {
        newStartingBrixFinal: newStartingBrix,
        sugarToAdd: 0.0,
        honeyToAdd: 0.0,
    };

    match allInputs.increaseABVUnitsEnum {
        imperialOrMetric::ImperialGB => {
            let sugarInOunces = (allInputs.currentVolume.gallonsGBToLitres() * 10.0 * differenceBrix).gramsToOunces();
            finalOutputFloat.sugarToAdd = sugarInOunces.ouncesToPounds();
            finalOutputFloat.honeyToAdd = sugarInOunces.sugarToHoney().ouncesToPounds();
        }
        imperialOrMetric::ImperialUS => {
            let sugarInOunces = (allInputs.currentVolume.gallonsUSToLitres() * 10.0 * differenceBrix).gramsToOunces();
            finalOutputFloat.sugarToAdd = sugarInOunces.ouncesToPounds();
            finalOutputFloat.honeyToAdd = sugarInOunces.sugarToHoney().ouncesToPounds();
        }
        imperialOrMetric::Metric => {
            finalOutputFloat.sugarToAdd = (allInputs.currentVolume * 10.0 * differenceBrix).gramsToKilograms();
            // Add 10g of sugar for every litre to raise 1°Bx
            finalOutputFloat.honeyToAdd = finalOutputFloat.sugarToAdd.sugarToHoney();
        }
    }

    finalOutputFloat
}

pub fn increaseABVFormatting(allInputs: &increaseABVData, finalOutputFloat: finalSugarFloat) -> sugarOutputStrings {
    let mut finalOutput: sugarOutputStrings = sugarOutputStrings {
        newStartingBrixFinal: format!("{:.2}°Bx", finalOutputFloat.newStartingBrixFinal),
        sugarAmount: String::from(""),
        honeyAmount: String::from(""),
    };

//  the reason for the allInputs.startingBrix.fract() + 0.001 is because rust interprets numbers
//  like 12.0 (after doing our maths) as 12.001.
    if allInputs.startingBrix.trunc() as u64 == finalOutputFloat.newStartingBrixFinal.trunc() as u64
    && allInputs.startingBrix.fract() + 0.001 >= finalOutputFloat.newStartingBrixFinal.fract() {
        finalOutput.sugarAmount = String::from("No need to");;
        finalOutput.honeyAmount = String::from("add any sugar");;
    } else {
        match allInputs.increaseABVUnitsEnum {
            imperialOrMetric::ImperialGB | imperialOrMetric::ImperialUS => {
                if finalOutputFloat.sugarToAdd < 0.995 {
                    finalOutput.sugarAmount = format!("{:.2} oz", finalOutputFloat.sugarToAdd.remainingOunces());
                } else if finalOutputFloat.sugarToAdd >= 0.995 && finalOutputFloat.sugarToAdd < 1.005 {
                    finalOutput.sugarAmount = format!("{:.0} lb", finalOutputFloat.sugarToAdd);
                } else if finalOutputFloat.sugarToAdd.trunc() as i64 == 1 && finalOutputFloat.sugarToAdd.remainingOunces() < 15.995 {
                    finalOutput.sugarAmount = format!("{:.0} lb {:.2} oz", finalOutputFloat.sugarToAdd.trunc(), finalOutputFloat.sugarToAdd.remainingOunces());
                } else if finalOutputFloat.sugarToAdd.fract() == 0.0 || finalOutputFloat.sugarToAdd.remainingOunces() < 0.005 || finalOutputFloat.sugarToAdd.remainingOunces() >= 15.995 {
                    finalOutput.sugarAmount = format!("{:.0} lbs", finalOutputFloat.sugarToAdd);
                } else {
                    finalOutput.sugarAmount = format!("{:.0} lbs {:.2} oz", finalOutputFloat.sugarToAdd.trunc(), finalOutputFloat.sugarToAdd.remainingOunces());
                }

                if finalOutputFloat.honeyToAdd < 0.995 {
                    finalOutput.honeyAmount = format!("{:.2} oz", finalOutputFloat.honeyToAdd.remainingOunces());
                } else if finalOutputFloat.honeyToAdd >= 0.995 && finalOutputFloat.honeyToAdd < 1.005 {
                    finalOutput.honeyAmount = format!("{:.0} lb", finalOutputFloat.honeyToAdd);
                } else if finalOutputFloat.honeyToAdd.trunc() as i64 == 1 && finalOutputFloat.honeyToAdd.remainingOunces() < 15.995 {
                    finalOutput.honeyAmount = format!("{:.0} lb {:.2} oz", finalOutputFloat.honeyToAdd.trunc(), finalOutputFloat.honeyToAdd.remainingOunces());
                } else if finalOutputFloat.honeyToAdd.fract() == 0.0 || finalOutputFloat.honeyToAdd.remainingOunces() < 0.005 || finalOutputFloat.honeyToAdd.remainingOunces() >= 15.995 {
                    finalOutput.honeyAmount = format!("{:.0} lbs", finalOutputFloat.honeyToAdd);
                } else {
                    finalOutput.honeyAmount = format!("{:.0} lbs {:.2} oz", finalOutputFloat.honeyToAdd.trunc(), finalOutputFloat.honeyToAdd.remainingOunces());
                }
            }
            imperialOrMetric::Metric => {
                if finalOutputFloat.sugarToAdd >= 0.995 && finalOutputFloat.sugarToAdd < 1.005 {
                    finalOutput.sugarAmount = format!("{:.0} kilo", finalOutputFloat.sugarToAdd);
                } else if finalOutputFloat.sugarToAdd.fract() >= 0.995 || finalOutputFloat.sugarToAdd.fract() < 0.005 {
                    finalOutput.sugarAmount = format!("{:.0} kilos", finalOutputFloat.sugarToAdd);
                } else {
                    finalOutput.sugarAmount = format!("{:.2} kilos", finalOutputFloat.sugarToAdd);
                }

                if finalOutputFloat.honeyToAdd >= 0.995 && finalOutputFloat.honeyToAdd < 1.005 {
                    finalOutput.honeyAmount = format!("{:.0} kilo", finalOutputFloat.honeyToAdd);
                } else if finalOutputFloat.honeyToAdd.fract() >= 0.995 || finalOutputFloat.honeyToAdd.fract() < 0.005 {
                    finalOutput.honeyAmount = format!("{:.0} kilos", finalOutputFloat.honeyToAdd);
                } else {
                    finalOutput.honeyAmount  = format!("{:.2} kilos", finalOutputFloat.honeyToAdd);
                }
            }
        }
    }

    finalOutput
}

fn increaseABVOutput(allInputs: increaseABVData, increaseABVBuilder: &gtk::Builder) {
    let increaseABVFinalBrix: gtk::Entry = increaseABVBuilder.get_object("increaseABVFinalBrix")
    .expect("increaseABVOutput(), increaseABVFinalBrix");

    let increaseABVSugar: gtk::Entry = increaseABVBuilder.get_object("increaseABVSugar")
    .expect("increaseABVOutput(), increaseABVFinalBrix");

    let increaseABVHoney: gtk::Entry = increaseABVBuilder.get_object("increaseABVHoney")
    .expect("increaseABVOutput(), increaseABVFinalBrix");

    let finalOutputFloat: finalSugarFloat = increaseABVMaths(&allInputs);
    let finalOutput: sugarOutputStrings = increaseABVFormatting(&allInputs, finalOutputFloat);

    increaseABVFinalBrix.set_text(&finalOutput.newStartingBrixFinal);
    increaseABVSugar.set_text(&finalOutput.sugarAmount);
    increaseABVHoney.set_text(&finalOutput.honeyAmount);
}